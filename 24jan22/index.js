var employee1 = {
    name : "Samir",
    address : "hyd",
    getDetails: function(){
        return this.name + "\t" + this.address;
    }
}
console.log(employee1.getDetails());

    // example2
function Customer(name, accountNo, balance){
    this.name = name;
    this.accountNo = accountNo;
    this.balance = balance;
    this.deposite = function(amount){
        this.balance = this.balance + amount;
    }
}
var customer1 = new Customer("samir",101,10000);
var customer2 = new Customer("maj", 102, 20000);
customer1.deposite(5200);
customer2.deposite(2);
